Rails.application.routes.draw do
  root 'home#index'
  post 'purchase' => "home#purchase"
  post 'handle_callback' => "home#handle_callback"
  get 'handle_redirect' => "home#handle_redirect"
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
